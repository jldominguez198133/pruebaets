import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app.module';
import {ROUTER_PROVIDERS} from '@angular/router'

platformBrowserDynamic().bootstrapModule(AppModule, [ROUTER_PROVIDERS]);
