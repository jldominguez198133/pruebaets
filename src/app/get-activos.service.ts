import { Injectable } from '@angular/core';
import { Headers, RequestOptions, Http, Response } from '@angular/http';
import {Observable} from "rxjs";

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class HTTPActivosService {
  constructor (private _http: Http) {}

  getActivos(): Observable<Array<any>> {
    let opt: RequestOptions;
    let myHeaders: Headers = new Headers;
    myHeaders.append('Content-type', 'application/json');
    myHeaders.append('JsonStub-User-Key', '9facef2e-9583-4a83-9f08-c87159f1c113');
    myHeaders.append('JsonStub-Project-Key', '6ed070c1-b334-4612-8fa8-169c5e45baef');
    opt = new RequestOptions({
      headers: myHeaders
    });

    return this._http.get('http://jsonstub.com/etsfintech/symbols', opt)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'))
  }
}
