import {Injectable, Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: "filter"
})
@Injectable()
export class SelectPipe implements PipeTransform {
  transform (value: Array<any>, args?: any) {
    let filterArray = [];
    let filterArray1 = [];
    if (args) {
      filterArray1 = value.filter(function(val, key) {
        if (val[args[0]] === args[1] && filterArray.indexOf(val[args[2]]) < 0) {
          filterArray.push(val[args[2]]);
          return true;
        } else {
          return false;
        }
      });
    } else {
      filterArray1 = value.filter(function(val, key) {
        if (filterArray.indexOf(val.batch) < 0) {
          filterArray.push(val.batch);
          return true;
        } else {
          return false;
        }
      });
    }
    return filterArray1;
  }
}
