import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { ActivosComponent } from './get-activos.component';
import { HTTPActivosService }          from './get-activos.service';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BaseChartDemoComponent } from './chart.component';

@NgModule({
  declarations: [
    AppComponent,
    ActivosComponent,
    BaseChartDemoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    ChartsModule
  ],
  providers: [
    HTTPActivosService
  ],
  bootstrap: [AppComponent, ActivosComponent, BaseChartDemoComponent]
})
export class AppModule { }
